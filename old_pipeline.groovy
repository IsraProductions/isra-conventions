#!groovy
{ ->
	node {
		stage('build') {
			try {
				cleanWs
				checkout scm
				sh './gradlew clean build --refresh-dependencies'
			} finally {
				junit 'build/reports/**/*.xml' //step([$class: 'Publisher'])
				jacoco
				archiveArtifacts allowEmptyArchive: true, artifacts: 'build/libs/**/*.jar', onlyIfSuccessful: true
			}
		}
	}
}