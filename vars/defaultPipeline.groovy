def call(body) {

	def config = [:]
	body.resolveStrategy = Closure.DELEGATE_FIRST
	body.delegate = config
	body()

	node {

		deleteDir()

		stage('Build') {
			try {
				checkout scm
				sh './gradlew clean build --refresh-dependencies -Pbranch=' + env.BRANCH_NAME
			} finally {
				archiveArtifacts allowEmptyArchive: true, artifacts: 'build/libs/**/*.jar', onlyIfSuccessful: true
				junit allowEmptyResults: true, testResults: 'build/reports/**/*.xml'
				try {
					jacoco()
				} catch(error) {
				}
			}
		}

		stage('Publish SNAPSHOT') {
			sh './gradlew publishJarPublicationToNexusRepository -Pbranch=' + env.BRANCH_NAME
		}

		stage('SonarQube') {
			withSonarQubeEnv('sonarqube') {
			  // requires SonarQube Scanner for Gradle 2.1+
			  // It's important to add --info because of SONARJNKNS-281
			  sh './gradlew --info sonarqube -Pbranch=' + env.BRANCH_NAME
			}
		}

	}
	
	stage("Quality Gate"){
		timeout(time: 1, unit: 'HOURS') { // Just in case something goes wrong, pipeline will be killed after a timeout
			def qualityGate = waitForQualityGate() // Reuse taskId previously collected by withSonarQubeEnv
			if (qualityGate.status != 'OK') {
				error "Pipeline aborted due to quality gate failure: ${qualityGate.status}"
			}
		}
	}

}